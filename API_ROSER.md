###Exemple de récupération des données via l'API 

https://roser.univ-tours.fr/omeka-s/api/items/5134key_identity=BYhKK5MPTPsBu4B3uShFNk9lKtwItTdPkey_credential=UC3w4VFwKG4jnKo4TiuqJ0vc3uYJ0Nu0?fbclid=IwAR2XBEyyZR0wbn5Hoe9kEq744SdDAadm-PdhFOn5c275KxXirIeR9Mmr3OU

### Première étape : repérer une notice d'ornement: 

Par exemple :
https://roser.univ-tours.fr/omeka-s/s/roser/item/5134

#### Accéder aux données brutes de la notice via l'API 

* Préfixe à utiliser : `https://roser.univ-tours.fr/omeka-s/api/items/`
* après `items/` noter l'ID de l'item dont on veut récupérer les données : ici `5134` 
    >l'ID de l'item se trouve toujours à la fin de l'URL (qui est un URI) de la page de sa notice : https://roser.univ-tours.fr/omeka-s/s/roser/item/ => ***5134***
* Puis ajouter la clef : `key_identity=BYhKK5MPTPsBu4B3uShFNk9lKtwItTdPkey_credential=UC3w4VFwKG4jnKo4TiuqJ0vc3uYJ0Nu0?fbclid=IwAR2XBEyyZR0wbn5Hoe9kEq744SdDAadm-PdhFOn5c275KxXirIeR9Mmr3OU`

*Ce qui donne : `https://roser.univ-tours.fr/omeka-s/api/items/5134key_identity=BYhKK5MPTPsBu4B3uShFNk9lKtwItTdPkey_credential=UC3w4VFwKG4jnKo4TiuqJ0vc3uYJ0Nu0?fbclid=IwAR2XBEyyZR0wbn5Hoe9kEq744SdDAadm-PdhFOn5c275KxXirIeR9Mmr3OU`

Un fichier JSON... Mais en fait comme il ne faut conserver que les propriétés pertinentes, et là c'est chaud. 