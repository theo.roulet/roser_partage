# ROSER_partageHNM1

Présentation du projet ROSER - [Répertoire des Ornements Sculpté des Eglises de la Renaissance](https://roser.univ-tours.fr/omeka-s/s/roser/page/welcome) - et partage des documents rendus à destination des étud- des poteaux en M1 Humanités Numériques au Ceus'rheu (CDC - Manuels - Rapport intégral final)
#

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">
<img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" />
</a><br />
Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">
Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 4.0 International</a>.

***

### Liste des participants :
* **Marie Petit**      étudiante en M2 MNCP      marie.petit@etu.univ-tours.fr
* **Florian Hivert**   étudiant en M2 IDCP      florian.hivert@etu.univ-tours.fr
* **Théo Roulet**      étudiant en M2 IDCP      theo.roulet@gmail.com
* **Jean Beuvier**     doctorant en Histoire de l'art au CESR. Thèse : ["L'ornement sculpté dans l'architecture religieuse française entre 1480 et 1540"](https://cesr.cnrs.fr/formations/doctorat/jean-beuvier) sous la direction de Marion Boudon Machuel


### ROSER est accessible en ligne : 
<a href="https://roser.univ-tours.fr/omeka-s/s/roser/page/welcome" >Répertoire de l'Ornement Sculpté des Eglises de la Renaissance </a> hébergé par l'Université de Tours, grâce à François Rosmann.


### Contenu du GITLAB :

* le rapport intégral en pdf (attention le fichier est lourd) 
* le cahier des charges ré-extrait du dossier 
* les manuels en html: à télécharger, avec le dossier images (ne casse pas le chemin relatif si tu veux voir les images)


### Description du projet :

Le projet R.O.S.E.R. vise à soutenir le travail d’inventaire, de numérisation et d’analyse
d’ornements architecturaux du XVe - XVIe siècle mené par Jean Beuvier, doctorant en Histoire de l’Art au CESR.

***En bref :***

Il a consisté à organiser en une base de données les données produites par Jean lors de la constitution de son corpus de recherche. Au moment du rendu du projet celles ci consistaient en quelques 200 photographies d’ornements sculptés (issus de 80 édifices français et italiens)  accompagnées de leur description et leur catégorisation effectuées par Jean Beuvier. 
Cette numérisation ne se limitait pas à la mise en forme d’un travail terminé, mais visait à la création d’un outil de gestion et de travail sur le corpus d’un travail de recherche en cours.

***Objectifs comme nous les avions définis dans notre cahiers des charges :***

> ROSER vise à permettre : 
>* Une meilleure consultation du corpus, à la fois en terme de visualisation des images des oeuvres numérisées et de leurs métadonnées. 
>* Un mécanisme d’annotation des images facilité et normalisé à l’aide de vocabulaires contrôlés, afin de systématiser le travail de description et de garantir l’interopérabilité des données produites. 
>* Un mécanisme d’interrogation du corpus, à partir de requêtes portant sur ces mêmes métadonnées, adapté aux besoins scientifiques d’analyse du corpus.
>* Une communication des données à un large public à travers un site web
>* L’intégration des données à d’autres programmes de recherches (grâce à l’interopérabilité de la base). 

### Le détail, l'histoire, et l'explication de notre démarche : 

La plus grande partie de notre travail a consisté, en collaboration avec Jean, à structurer ses données, c’est à dire : 
* à créer un modèle conceptuel de base de données qui puisse traduire sa démarche ce qui voulait dire reprendre les informations qu’il avait déjà produites mais aussi accueillir et faciliter ses futures analyses. 
* à déterminer les langages dans lesquels exprimer ces données (quels vocabulaire de description de l’iconographie ? de l’architecture ? Quelle ontologie RDF-S employer dans la base pour décrire les liens entre les différentes entités). 
* à nous assurer que ces langages rendent la base interopérable (au moins en théorie). 

Puis nous avons choisi le CMS avec lequel déployer la base (OMEKA S) et son interface de gestion / de publication web (en grande partie déjà disponible grâce au CMS). Nous avons alors lutté avec pour faire en sorte de la rendre la plus proche de nos objectifs initiaux :

* réussir à la faire respecter notre modèle, utiliser certains langages / ontologies dedans n’a pas été facile, nous avons dû faire un certain nombre de compromis (et puis nous avons réalisé au fur et à mesure que nous avions mal compris certaines choses, que d’autres n’étaient pas possibles, etc. Là encore, rencontrer des professionnels qui travaillaient avec Omeka S nous a beaucoup aidé) . 

* Aménager l’interface au mieux (modification du thème, paramétrage des outils de recherche, affichage des images et des métadonnées …) : ne t’inquiète pas, au début c’était bien plus fruste que ça ne l'est devenu aujourd’hui (heures sup' l'été). 

* Dernière étape du projet : rédiger les manuels utilisateurs et notre dossier. 

> Pfiou ! C’est fini ! Hé ben non, nous continuons encore à travailler dessus, car Jean s’en sert vraiment, un certain nombre de questions restaient ouvertes, elles correspondent au cours de M2 IDCP, et puis nous ressentions un certain sentiment d’inachevé. 

### Les retours : 

##### Négatifs : 
* La mise en forme du dossier en colonnes : soyez classiques, copiez la mise en forme d’une réponse à un Appel à Projet. 

##### Positifs : 
* Ils ont aimé les liens du projet avec nos cours, en particulier les questions d’interopérabilité, qui sont aussi au coeur de toutes les entreprises de numérisation / publication du travail des chercheurs dans tous les domaines aujourd’hui (mais aussi des bibliothèques, des musées …). 
* Ils ont apprécié qu’il existe un prototype fonctionnel. 

### Conseils : 

* Rédigez vos parties au fur et à mesure : votre notre d’intention / votre objectif scientifique vous servira d’introduction, votre cahier des charges vous donnera la trame, vos analyses de certains problèmes / vos analyses de projets similaires vous serviront d’état de l’art, etc. Même vos comptes rendus de réunions pourront presques être réutilisés tels quels. 

* Vous êtes jugés par des profs de SHS : il ne faut pas oublier l’état de l’art et la bibliographie (le nôtre n’est pas terrible d’ailleurs), on doit justifier tous nos choix. 


* Gérez bien la documentation : toutes les informations que vous accumulez au fur et à mesure (pour certaines choses, j’en pleure encore …). 

* Pour la documentation et la bonne organisation du dossier : regardez ce qu’à fait Diandra, c’est béton. 


* N’hésitez pas non plus à rédiger des fragments de vos manuels utilisateurs à l’avance (par exemple, quand vous choisirez vos outils / logiciels / CMS : faites le au rythme auquel vous apprenez à vous en servir, ça vous sera doublement utile. 


* Cherchez à rencontrer des pros si vous le pouvez : ils sont souvent super enthousiastes quand des étudiants font les mêmes choses qu’eux. Au mieux ils vous aident, au pire ils vous apprennent au moins le vocabulaire pour parler correctement de toutes les choses compliquées que recouvrent les Humanités Numériques 

* Postez sur les forums : ça marche, on ne l’a pas assez fait. 




